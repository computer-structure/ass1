#include <stdlib.h>
#include <stdio.h>
#include "bytes.h"

static int count = 0;

byte_t *create_bytes(char *file_name) {
    FILE *fp;
    fp = fopen(file_name, "r");
    if (fp == NULL) {
        printf("Error!");
        exit(1);
    }
    byte_t *newAns = NULL, *newAnsTemp = NULL;

    int ans1, ans2, ans3, ans4;
    while (fscanf(fp, "%d,%d,%d,%d\n", &ans1, &ans2, &ans3, &ans4) == 4){
        byte_t m0 = 0b0000;
        byte_t m1 = 0b0001;
        byte_t m2 = 0b0010;
        byte_t m3 = 0b0011;
        byte_t x = 0b0000;

        if (newAns == NULL) {
            newAns = (byte_t *) malloc(sizeof(byte_t));
            if (newAns == NULL) {
                printf("error\n");
                exit(1);
            }
            count = 1;
        }
        else{
            newAnsTemp = (byte_t *) realloc(newAns, (count + 1) * sizeof(byte_t));
            if (newAnsTemp == NULL){
                printf("error\n");
                exit(1);
                }
            newAns = newAnsTemp;
            newAnsTemp = NULL;
            count++;
            }

        switch (ans4){
            case 0:
                x = x | m0;
                break;

            case 1:
                x = x | m1;
                break;

            case 2:
                x = x | m2;
                break;

            case 3:
                x = x | m3;
                break;

            default:
                break;
        }
        x <<= 2u;

        switch (ans3){
            case 0:
                x = x | m0;
                break;

            case 1:
                x = x | m1;
                break;

            case 2:
                x = x | m2;
                break;

            case 3:
                x = x | m3;
                break;

            default:
                break;
        }
        x <<= 2u;

        switch (ans2){
            case 0:
                x = x | m0;
                break;

            case 1:
                x = x | m1;
                break;

            case 2:
                x = x | m2;
                break;

            case 3:
                x = x | m3;
                break;

            default:
                break;
        }
        x <<= 2u;

        switch (ans1){
            case 0:
                x = x | m0;
                break;

            case 1:
                x = x | m1;
                break;

            case 2:
                x = x | m2;
                break;

            case 3:
                x = x | m3;
                break;

            default:
                break;
        }
        newAns[count - 1] = x;
    }
    fclose(fp);
    return newAns;
}

void print_bytes(byte_t *byte_array, FILE *out) {
    for (int i = 0; i < count; i++) {
        fprintf(out, "%.2x", byte_array[i]);
    }
    printf("\n");
}

void set_stud(byte_t *byte_array, int i, int j, int k) {
    byte_t m0 = 0b0000;
    byte_t m1 = 0b0001;
    byte_t m2 = 0b0010;
    byte_t m3 = 0b0011;
    byte_t x = 0b0000;

    switch (k){
        case 0:
            x |= m0;
            break;

        case 1:
            x |= m1;
            break;

        case 2:
            x |= m2;
            break;

        case 3:
            x |= m3;
            break;

        default:
            break;
    }
    x <<= 2*(j - 1);
    byte_t c1 = 0b11111100;
    byte_t c2 = 0b11110011;
    byte_t c3 = 0b11001111;
    byte_t c4 = 0b00111111;

    switch (j){
        case 1:
            byte_array[i - 1] &= c1;
            break;

        case 2:
            byte_array[i - 1] &= c2;
            break;

        case 3:
            byte_array[i - 1] &= c3;
            break;

        case 4:
            byte_array[i - 1] &= c4;
            break;

        default:
            break;
    }
    byte_array[i - 1] |= x;
}

float average_stud(byte_t *byte_array, int i) {
    byte_t m = 0b00000011;
    byte_t temp1;
    float sum;
    float temp2;
    for (int j = 0; j < 4; j++){
        temp1 = byte_array[i - 1];
        temp1 = byte_array[i - 1] & m;
        temp1 >>= 2*j;
        temp2 = (float) temp1;
        sum += temp2;
        m <<= 2;
    }
    return sum/4;
}

float average_ans(byte_t *byte_array, int j) {
    byte_t m = 0b0011;
    byte_t temp1;
    float temp2;
    float sum = 0;
    m <<= 2*(j - 1);
    for (int i = 0; i < count; i++){
        temp1 = byte_array[i];
        temp1 &= m;
        temp1 >>= 2*(j - 1);
        temp2 = (float) temp1;
        sum += temp2;
    }
    return sum/count;
}

